{ pkgs, ... }:
let
  pname = "material-cursors";
  material-cursors-git = pkgs.fetchFromGitHub {
    owner = "varlesh";
    repo = "material-cursors";
    rev = "2a5f302fefe04678c421473bed636b4d87774b4a";
    sha256 = "uC2qx3jF4d2tGLPnXEpogm0vyC053MvDVVdVXX8AZ60=";
  };
in pkgs.stdenv.mkDerivation {
  inherit pname;
  version = material-cursors-git.rev;
  src = material-cursors-git;
  buildInputs = with pkgs; [ git inkscape libcanberra xorg.xcursorgen ];
  buildPhase = "export NO_AT_BRIDGE=1; make build";
  installPhase =
    ''make DESTDIR="$out" install; mv $out/usr/share $out/; rmdir $out/usr'';
}
