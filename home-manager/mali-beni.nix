{ config, pkgs, lib, ... }: {
  imports = [ ./. ./hyprland.nix ];

  nixpkgs.config.allowUnfreePredicate = pkgs:
    builtins.elem (lib.getName pkgs) [ "vscode" ];

  home.file = {
    ".gnupg/gpg-agent.conf".text = ''
      pinentry-program ${pkgs.pinentry-qt}/bin/pinentry-qt
    '';
  };

  home.packages = with pkgs; [
    htop
    libsForQt5.ksshaskpass
    neofetch
    rnix-lsp
    starship
    vscode
  ];

  home.sessionVariables = {
    SSH_ASKPASS = lib.mkForce "${pkgs.libsForQt5.ksshaskpass}/bin/ksshaskpass";
  };

  programs = {
    btop.package = lib.mkForce pkgs.btop;
    git = {
      package = lib.mkForce pkgs.git;
      signing = { gpgPath = lib.mkForce "${pkgs.gnupg}/bin/gpg2"; };
    };
    gpg.enable = true;
    kitty.package = lib.mkForce pkgs.kitty;
    man.enable = true;
    starship = {
      package = lib.mkForce pkgs.starship;
      enableZshIntegration = lib.mkForce true;
    };
    vscode.enable = true;
    zsh = {
      package = lib.mkForce pkgs.zsh;
      initExtra = lib.mkForce ''
        neofetch
      '';
    };
  };

  services.syncthing.tray = {
    enable = true;
    command = "syncthingtray --wait";
  };

  xsession = {
    enable = true;
    numlock.enable = true;
  };

  wayland.windowManager.hyprland = { enableNvidiaPatches = true; };
}
