{ pkgs, ... }:
let
  material-cursors = import ../packages/material-cursors.nix { inherit pkgs; };
in {
  home.file = {
    ".local/bin/hypr-killactive.sh" = {
      executable = true;
      text = ''
        if [ "$(hyprctl activewindow -j | jq -r ".class")" = "Steam" ]; then
            xdotool getactivewindow windowunmap
        else
            hyprctl dispatch killactive ""
        fi
      '';
    };
  };

  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    libsForQt5.dolphin
    libsForQt5.kwalletmanager
    udiskie
    pinentry_qt5
    qt6Packages.qt6ct
  ];

  home.pointerCursor = {
    package = material-cursors;
    gtk.enable = true;
    name = "material_light_cursors";
    size = 24;
    x11.enable = true;
  };

  gtk = {
    enable = false;
    cursorTheme = {
      name = "material_light_cursors";
      size = 24;
      package = material-cursors;
    };
    font = {
      name = "Noto Sans";
      size = 10;
      package = (pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; });
    };
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };
    theme = {
      name = "Materia-dark";
      package = pkgs.materia-theme;
    };
  };

  qt = {
    enable = false;
    platformTheme = "qtct";
    style = {
      name = "kvantum";
      package = pkgs.qt6Packages.qtstyleplugin-kvantum;
    };
  };

  home.sessionVariables = {
    QT_QPA_PLATFORM = "wayland;xcb";
    SDL_VIDEODRIVER = "wayland";
    CLUTTER_BACKEND = "wayland";
    QT_AUTO_SCREEN_SCALE_FACTOR = "1";
    QT_WAYLAND_DISABLE_WINDOWDECORATIONS = "1";
    MOZ_ENABLE_WAYLAND = "1";
    MOZ_DISABLE_RDD_SANDBOX = "1";
    EGL_PLATFORM = "wayland";
  };

  wayland.windowManager.hyprland = {
    enable = true;
    settings = {
      monitor = "eDP-1,1920x1080@144,0x0,1";

      input = {
        kb_layout = "us";

        follow_mouse = 1;

        touchpad.natural_scroll = false;

        sensitivity = 0;
      };

      general = {
        gaps_in = 4;
        gaps_out = 10;
        border_size = 1;
        "col.active_border" = "rgba(33ccffee) rgba(00ff99ee) 45deg";
        "col.inactive_border" = "rgba(595959aa)";

        resize_on_border = true;

        layout = "master";
      };

      decoration = {
        rounding = 4;
        multisample_edges = true;
        blur = true;
        blur_size = 3;
        blur_passes = 1;
        blur_new_optimizations = true;

        drop_shadow = true;
        shadow_range = 4;
        shadow_render_power = 3;
        "col.shadow" = "rgba(1a1a1aee)";
      };

      animations = {
        enabled = true;

        bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";

        animation = [
          "windows, 1, 7, myBezier"
          "windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "borderangle, 1, 8, default"
          "fade, 1, 7, default"
          "workspaces, 1, 6, default"
        ];
      };

      dwindle = {
        pseudotile = true;
        preserve_split = true;
      };

      master = {
        new_is_master = true;
        no_gaps_when_only = true;
      };

      gestures.workspace_swipe = false;

      "$mainMod" = "SUPER";

      bind = [
        "$mainMod, Return, exec, kitty"
        "$mainMod, X, exec, ~/.local/bin/hypr-killactive.sh"
        "SUPER_SHIFT, Q, exit,"
        "$mainMod, E, exec, dolphin"
        "$mainMod, V, togglefloating,"
        "$mainMod, R, exec, wofi --allow-images --show drun"

        # Master Layout
        "$mainMod, comma, focusmonitor, -1"
        "$mainMod, period, focusmonitor, +1"

        # Move focus with mainMod + arrow keys
        "$mainMod, H, movefocus, l"
        "$mainMod, L, movefocus, r"
        "$mainMod, K, movefocus, u"
        "$mainMod, J, movefocus, d"
        "$mainMod, P, layoutmsg, swapwithmaster auto"

        # Switch workspaces with mainMod + [0-9]
        "$mainMod, 1, workspace, 1"
        "$mainMod, 2, workspace, 2"
        "$mainMod, 3, workspace, 3"
        "$mainMod, 4, workspace, 4"
        "$mainMod, 5, workspace, 5"
        "$mainMod, 6, workspace, 6"
        "$mainMod, 7, workspace, 7"
        "$mainMod, 8, workspace, 8"
        "$mainMod, 9, workspace, 9"
        "$mainMod, 0, workspace, 10"

        # Move active window to a workspace with mainMod + SHIFT + [0-9]
        "$mainMod SHIFT, 1, movetoworkspace, 1"
        "$mainMod SHIFT, 2, movetoworkspace, 2"
        "$mainMod SHIFT, 3, movetoworkspace, 3"
        "$mainMod SHIFT, 4, movetoworkspace, 4"
        "$mainMod SHIFT, 5, movetoworkspace, 5"
        "$mainMod SHIFT, 6, movetoworkspace, 6"
        "$mainMod SHIFT, 7, movetoworkspace, 7"
        "$mainMod SHIFT, 8, movetoworkspace, 8"
        "$mainMod SHIFT, 9, movetoworkspace, 9"
        "$mainMod SHIFT, 0, movetoworkspace, 10"

        # Scroll through existing workspaces with mainMod + scroll
        "$mainMod, mouse_down, workspace, e+1"
        "$mainMod, mouse_up, workspace, e-1"

        # Move/resize windows with mainMod + LMB/RMB and dragging
        "$mainMod, mouse:272, movewindow"
        #"$mainMod, mouse:273, resizewindow"
      ];
    };
    systemdIntegration = true;
    xwayland.enable = true;
  };

  services = {
    mako = {
      enable = true;
      layer = "overlay";
    };
    udiskie = {
      enable = true;
      automount = false;
    };
  };

  programs = {
    waybar = {
      enable = true;
      package = pkgs.waybar.overrideAttrs (oldAttrs: {
        mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
      });
      systemd = {
        enable = true;
        target = "hyprland-session.target";
      };
    };
    wofi = { enable = true; };
  };
}
