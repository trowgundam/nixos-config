{ config, pkgs, lib, nixvim-config, vscode-server, xivlauncherotp, ... }:
let dummy-pkg = pkgs.hello;
in {
  imports = [ "${vscode-server}/modules/vscode-server/home.nix" ];

  targets.genericLinux.enable = true;

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.

  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "jeff";
  home.homeDirectory = "/home/jeff";
  home.sessionPath = [ "$HOME/.cargo/bin" ];
  home.sessionVariables = {
    EDITOR = "nvim";
    SSH_ASKPASS = "ksshaskpass";
    SSH_ASKPASS_REQUIRE = "prefer";
    XL_PATH = "$HOME/.local/share/xlcore";
  };
  home.shellAliases = {
    cat = "bat";
    cdiff = "bat -d";
    l = "exa -l";
    lg = ''
      exa -lah --icons -s type --tree -R --git --git-ignore --ignore-glob=".git"'';
    ll = "exa -lah --icons -s type";
    ls = "exa";
    mkdir = "mkdir -p";
    rm = "rm -i";
    ".." = "cd ..";
    "..." = "cd ../..";
  };

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    nixvim-config.packages.${system}.default
    xivlauncherotp.packages.${system}.default
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    ".config/brave-flags.conf".text = ''
      --ignore-gpu-blocklist
      --enable-gpu-rasterization
      --enable-zero-copy
      --enable-features=VaapiVideoDecoder,VaapiIgnoreDriverChecks
      --ozone-platform-hint=auto
    '';
  };

  home.language.base = "en_US.UTF-8";

  manual.manpages.enable = true;

  #nix.settings = {
  #  experimental-features = [ "nix-command" "flakes" ];
  #  keep-derivations = true;
  #  keep-outputs = true;
  #  max-jobs = "auto";
  #};

  xdg.userDirs.enable = true;
  xdg.userDirs.createDirectories = true;

  services.ssh-agent.enable = true;
  services.vscode-server.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  programs = {
    btop = {
      enable = true;
      package = dummy-pkg;
    };
    command-not-found.enable = true;
    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
    git = {
      enable = true;
      package = dummy-pkg;
      userName = "Jeffrey Geer";
      userEmail = "viriveri@gmail.com";
      signing = {
        key = "98C059E39428ADB1";
        signByDefault = true;
        gpgPath = "/usr/bin/gpg";
      };
      lfs.enable = true;
    };
    kitty = {
      enable = true;
      package = dummy-pkg;
      font = {
        package = (pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; });
        name = "FiraCode Nerd Font";
        size = 11.0;
      };
      settings = { background_opacity = "0.95"; };
      theme = "Material Dark";
    };
    starship = {
      enable = true;
      package = dummy-pkg;
      enableZshIntegration = false;
      settings = {
        aws.symbol = "  ";
        buf.symbol = " ";
        c.symbol = " ";
        conda.symbol = " ";
        dart.symbol = " ";
        directory.read_only = " 󰌾";
        docker_context.symbol = " ";
        elixir.symbol = " ";
        elm.symbol = " ";
        fossil_branch.symbol = " ";
        git_branch.symbol = " ";
        golang.symbol = " ";
        guix_shell.symbol = " ";
        haskell.symbol = " ";
        haxe.symbol = "⌘ ";
        hg_branch.symbol = " ";
        hostname.ssh_symbol = " ";
        java.symbol = " ";
        julia.symbol = " ";
        lua.symbol = " ";
        memory_usage.symbol = "󰍛 ";
        meson.symbol = "󰔷 ";
        nim.symbol = "󰆥 ";
        nix_shell.symbol = " ";
        nodejs.symbol = " ";

        os.symbols = {
          Alpaquita = " ";
          Alpine = " ";
          Amazon = " ";
          Android = " ";
          Arch = " ";
          Artix = " ";
          CentOS = " ";
          Debian = " ";
          DragonFly = " ";
          Emscripten = " ";
          EndeavourOS = " ";
          Fedora = " ";
          FreeBSD = " ";
          Garuda = "󰛓 ";
          Gentoo = " ";
          HardenedBSD = "󰞌 ";
          Illumos = "󰈸 ";
          Linux = " ";
          Mabox = " ";
          Macos = " ";
          Manjaro = " ";
          Mariner = " ";
          MidnightBSD = " ";
          Mint = " ";
          NetBSD = " ";
          NixOS = " ";
          OpenBSD = "󰈺 ";
          openSUSE = " ";
          OracleLinux = "󰌷 ";
          Pop = " ";
          Raspbian = " ";
          Redhat = " ";
          RedHatEnterprise = " ";
          Redox = "󰀘 ";
          Solus = "󰠳 ";
          SUSE = " ";
          Ubuntu = " ";
          Unknown = " ";
          Windows = "󰍲 ";
        };

        package.symbol = "󰏗 ";
        pijul_channel.symbol = "🪺 ";
        python.symbol = " ";
        rlang.symbol = "󰟔 ";
        ruby.symbol = " ";
        rust.symbol = " ";
        scala.symbol = " ";
        spack.symbol = "🅢 ";
      };
    };
    zsh = {
      enable = true;
      package = dummy-pkg;
      autocd = true;
      enableAutosuggestions = true;
      defaultKeymap = "viins";
      dotDir = ".config/zsh";
      history = {
        ignoreDups = true;
        path = "${config.xdg.configHome}/zsh/.histfile";
        save = 1000;
        size = 1000;
      };
      initExtraFirst = ''
        setopt extendedglob nomatch notify
        unsetopt beep
      '';
      initExtra = ''
        neofetch
        if [[ $TERM != "dumb" ]]; then
          eval "$(starship init zsh)"
        fi
      '';
      initExtraBeforeCompInit = let dq = "''";
      in ''
        zstyle ':completion:*' auto-description 'specify: %d'
        zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
        zstyle ':completion:*' format 'Completing %d'
        zstyle ':completion:*' group-name ${dq}
        zstyle ':completion:*' ignore-parents pwd
        zstyle ':completion:*' list-colors ${dq}
        zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
        zstyle ':completion:*' list-suffixes true
        zstyle ':completion:*' matcher-list ${dq} '+m:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
        zstyle ':completion:*' max-errors 2
        zstyle ':completion:*' menu select=long
        zstyle ':completion:*' preserve-prefix '//[^/]##/'
        zstyle ':completion:*' prompt 'Errors %e'
        zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
        zstyle ':completion:*' squeeze-slashes true
        zstyle ':completion:*' use-compctl true
        zstyle :compinstall filename '/home/jeff/.config/zsh/.zshrc'
      '';
      syntaxHighlighting.enable = true;
    };
  };
}
