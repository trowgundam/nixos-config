# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, hostName, ... }:

{
  nix.settings = {
    #auto-optimize-store = true;
    keep-outputs = true;
    keep-derivations = true;

    experimental-features = [ "nix-command" "flakes" ];
  };
  nixpkgs.config.allowUnfree = true;

  #boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.loader = {
    grub = {
      enable = true;
      efiSupport = true;
      device = "nodev";
    };
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/efi";
    };
  };

  networking.hostName = hostName; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable =
    true; # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    #   font = "Lat2-Terminus16";
    keyMap = "us";
    #   useXkbConfig = true; # use xkbOptions in tty.
  };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jeff = {
    description = "Jeffrey Geer";
    hashedPassword =
      "$y$j9T$PUNq3so6xC2Rmq8B3vRip0$n1yebf0BErY1JoBYYFyxCM6xrBHmbGM4dTAOBmEfMy1";
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAjdM4vnqmeSY3ywDT8sKk+tPzoUyreZpq2IqKAS0DDT6PRpiwRessCGgl5QNBIkxRwEe2ip1GnHM8W1Cg2hb5kJkumVzJeUbUsW8EClT5G30uKiJBVmXCgy7nbFT5pJjNLd6bXdW6T/u8N2bKOSi8A8ro0a7bpvN3bXFJcDOS46+K6x/kKC1jO7OOyC3CPjOHpyOGXnUjzfSLeXB/nUfuONOzLmPtoJotOZ8Ec0GhUo82eY4wjvc9qEbJpsQXx0SUAmFCpS2+C6rHAcr5F6z0gjn7yUfDdYpIJ2qM3U+ZF1FYCo1HE1750JiR38sj2KZ93Wxl0BNmcjoPEeUs3tw/jpUQHzpYVXUNqhiZX6diwKEEx/v47xxizS6u2Mc/ddw07kFYwrzuWl/ron+1TJ8g8m16tSSdnGLAMfuRK1p4Rw6MirO5gldbOVXmTCAoLyOuU+aTOrHtA5+n/gPq3fE4JuJECPLe/LsH5FPLeYTI9f4fz8+y/dn9Ir/sUiWmKa5CiuJLFg9+Pm6ovsdiT3BmgO8vMuyYEDSp3uQq7Gr8uZM8tkzHBc6SVUNeTlOVMg09c0huGGuHooZrIT1rKVnaROK+7OJvsjQBW96FTeqsRSHYh6Kla1OEcSWmR6a3LqLaQdO7h6LPgKcWzgfvSCCOxkH3LbROI/rRExEHUw9JPl8="
    ];
    packages = with pkgs; [ ];
  };

  environment.shellAliases = {
    cat = "bat";
    cdiff = "bat -d";
    l = "exa -l";
    lg = ''
      exa -lah --icons -s type --tree -R --git --git-ignore --ignore-glob=".git"'';
    ll = "exa -lah";
    ls = "exa";
    mkdir = "mkdir -p";
    rm = "rm -i";
  };

  environment.variables = { EDITOR = "nvim"; };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    bat
    btop
    exa
    gcc
    htop
    neovim
    nfs-utils
    nixfmt
    p7zip
    rnix-lsp
    starship
    vim
    xdg-user-dirs
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  programs.git.enable = true;
  programs.starship.enable = true;
  programs.zsh.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
  };

  services.rpcbind.enable = true;
  systemd = {
    mounts = [{
      type = "nfs";
      mountConfig = { Options = "noatime"; };
      what = "172.16.2.1:/mnt/user";
      where = "/mnt/Unraid";
    }];
    automounts = [{
      wantedBy = [ "multi-user.target" ];
      automountConfig = { TimeoutIdleSec = "600"; };
      where = "/mnt/Unraid";
    }];
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}

