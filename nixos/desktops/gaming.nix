{ config, pkgs, lib, ... }: {
  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [ "steam" "steam-original" "steam-run" ];
  nixpkgs.overlays = [
    (final: prev: {
      steam = prev.steam.override ({ extraPkgs ? pkgs': [ ], ... }: {
        extraPkgs = pkgs': (extraPkgs pkgs') ++ (with pkgs'; [ libgdiplus ]);
      });
    })
  ];

  environment.systemPackages = with pkgs; [
    heroic
    lutris
    protontricks
    protonup-qt
    wine-staging
    wineWowPackages.stagingFull
    winetricks
    xivlauncher
  ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
  };
}
