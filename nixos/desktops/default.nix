{ config, pkgs, ... }:
let
  material-cursors =
    import ../../packages/material-cursors.nix { inherit pkgs; };
in {
  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    audio.enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    wireplumber.enable = true;
  };

  environment.systemPackages = with pkgs; [
    brave
    kitty
    libsForQt5.qtstyleplugin-kvantum
    libva-utils
    materia-kde-theme
    materia-theme
    material-cursors
    papirus-icon-theme
    pinentry
    pinentry-qt
    rustdesk
  ];

  fonts = {
    enableDefaultPackages = true;
    fontconfig = {
      defaultFonts = {
        serif = [ "Noto Serif" ];
        sansSerif = [ "Noto Sans" ];
        monospace = [ "FiraCode Nerd Font" ];
      };
    };
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji

      (nerdfonts.override { fonts = [ "FiraCode" ]; })
    ];
  };

  xdg.portal.enable = true;
  services.flatpak.enable = true;

  services.syncthing = {
    enable = true;
    user = "jeff";
    configDir = "/home/jeff/.config/syncthing";
    dataDir = "/home/jeff/Sync";
  };
}
