{ config, pkgs, material-cursors-pkg, ... }: {
  imports = [ ./. ];

  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  services.xserver = {
    enable = true;
    displayManager.defaultSession = "hyprland";
    displayManager.sddm = {
      enable = true;
      theme = "materia-dark";
      autoNumlock = true;
      settings = { Theme.CursorTheme = "material_light_cursors"; };
    };
  };

  security.pam.services.jeff.enableKwallet = true;

  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };

  environment.systemPackages = with pkgs; [
    libsForQt5.polkit-kde-agent
    libsForQt5.qt5.qtgraphicaleffects
    libsForQt5.kwallet
    libsForQt5.kwallet-pam
  ];

  systemd = {
    user.services.polkit-kde-authentication-agent-1 = {
      description = "polkit-kde-authentication-agent-1";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart =
          "${pkgs.libsForQt5.polkit-kde-agent}/libexec/polkit-kde-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  };
}
