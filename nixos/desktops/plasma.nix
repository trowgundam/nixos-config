{ config, pkgs, material-cursors-pkg, ... }: {
  imports = [ ./. ];

  services.xserver = {
    enable = true;
    displayManager.defaultSession = "plasmawayland";
    displayManager.sddm = {
      enable = true;
      theme = "materia-dark";
      autoNumlock = true;
      settings = { Theme.CursorTheme = "material_light_cursors"; };
    };
    desktopManager.plasma5.enable = true;
  };

  programs.dconf.enable = true;

  environment.plasma5.excludePackages = with pkgs.libsForQt5; [ konsole ];

  environment.systemPackages = with pkgs; [
    libsForQt5.discover
    libsForQt5.ark
  ];
}
