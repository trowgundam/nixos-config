{
  description = "TrowGundam's NixOS + home-manager config";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:nixos/nixos-hardware/master";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    vscode-server = {
      url = "github:msteen/nixos-vscode-server";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixvim-config = {
      url = "gitlab:trowgundam/nixvim-config";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    xivlauncherotp = {
      url = "gitlab:trowgundam/xivlauncherotp";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, nixos-hardware, nixvim-config
    , vscode-server, xivlauncherotp, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      formatter.${system} = pkgs.nixfmt;

      nixosConfigurations = let
        nixos-virt-manager = "nixos-virt-manager";
        mali-beni = "mali-beni";
      in {
        ${nixos-virt-manager} = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            ./nixos/hardware/${nixos-virt-manager}.nix
            ./nixos
            ./nixos/desktops/plasma.nix
            home-manager.nixosModules.home-manager
            {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                users.jeff = import ./home-manager/mali-beni.nix;
                extraSpecialArgs = { inherit nixvim-config vscode-server; };
              };
            }
          ];
          specialArgs = { hostName = nixos-virt-manager; };
        };
        ${mali-beni} = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            ./nixos/hardware/${mali-beni}.nix
            nixos-hardware.nixosModules.common-cpu-intel
            nixos-hardware.nixosModules.common-gpu-nvidia
            nixos-hardware.nixosModules.common-pc-laptop
            ./nixos
            ./nixos/desktops/hyprland.nix
            ./nixos/desktops/gaming.nix
            home-manager.nixosModules.home-manager
            {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                users.jeff = import ./home-manager/mali-beni.nix;
                extraSpecialArgs = {
                  inherit nixvim-config vscode-server xivlauncherotp;
                };
              };
            }
          ];
          specialArgs = { hostName = mali-beni; };
        };
      };

      homeConfigurations = {
        "jeff" = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;

          # Specify your home configuration modules here, for example,
          # the path to your home.nix.
          modules = [ ./home-manager ];

          # Optionally use extraSpecialArgs
          # to pass through arguments to home.nix
          extraSpecialArgs = {
            inherit nixvim-config vscode-server xivlauncherotp;
          };
        };
      };
    };
}
